const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3000;

mongoose.connect(`mongodb+srv://RMS:Admin123@zuitt-batch197.kblestk.mongodb.net/s35c1?retryWrites=true&w=majority`, {

	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection


db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connected to MongoDB!'));

const userSchema = new mongoose.Schema({
	firstName : String,
	lastName : String,
	username: String,
	password : String,
	email : String
})

const productSchema = new mongoose.Schema({
	name : String,
	description : String,
	price: Number
})

const User = mongoose.model('User', userSchema);
const Product = mongoose.model('Product', productSchema);


app.use(express.json());
app.use(express.urlencoded({extended:true}));
// 1
app.post('/register', (req, res) => {
	User.findOne({username: req.body.username},(err, result) =>{
		if(result !==null && result.username === req.body.username){
			return res.send("Username already taken!")
		} else {
			let newUser = new User ({
				firstName:req.body.firstName,
				lastName: req.body.lastName,
				username: req.body.username,
				password: req.body.password,
				email: req.body.email
			})

			newUser.save((saveErr, savedUser) => {
				console.log(savedUser)

				if(saveErr) {
					return console.err(saveErr)
				} else {
					return res.send("New user registered")
				}

			})
		}
	})
});
//2
app.post('/createProduct', (req, res) => {
	let newProduct = new Product ({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})
	newProduct.save()
	.then(product => res.send(product))
});


// 3
app.get('/users', (req,res) => {
	User.find({})
	.then(result => res.send(result))
});
	
//4
app.get('/products', (req,res) => {
	Product.find({})
	.then(result => res.send(result))
});


app.listen(port, () => console.log(`Server is running at port: ${port}`));